const express = require('express')
const app = express()

app.get('/', (req, res, next) => {
  res.json({ message: 'Hello Piyanut' })
})
app.listen(9000, () => {
  console.log('Application is running on port 9000')
})